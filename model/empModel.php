<?php
require_once 'db/dbhelper.php';
Class Employee extends DBHelper{
    // private $today= date("Y-m-d");
    private $table = 'tbl_employee';
    private $fields = array(
        'emp_id',
        'emp_fname',
        'emp_lname',
        'emp_bdate',
        'emp_hw',
        'emp_rate',
        'emp_ot',
    );
    private $fields2 = array(
        'emp_id',
        'emp_fname',
        'emp_lname',
        'CURDATE() - emp_bdate',
        'emp_hw',
        'emp_rate',
        'emp_ot',
    );

//constructo
    function __construct(){
        return DBHelper::__construct();
    }
// Create
function addEmployee($data){
    return DBHelper::insertRecord($data,$this->fields,$this->table); 
 }
// Retreive
 function getAllEmployee(){
     return DBHelper::getAllRecord($this->table,"emp_id");
 }
 function getAllEmployeeByFullname(){
    return DBHelper::getAllRecordOrder($this->table,"emp_lname");
}
function getAllEmployeeByBday(){
    return DBHelper::getAllRecordOrder($this->table,"emp_bdate");
}
function getAllEmployeeBySearch($data){
    return DBHelper::getAllRecordSearch($this->table,$data,$this->fields2);
}
function getAllEmployeeByOP(){
    return DBHelper::getAllRecordOrder($this->table,"(emp_rate*emp_hw)+(emp_rate*emp_ot)");
}
//----
 function getEmployeeById($ref_id){
    return DBHelper::getRecordById($this->table,'emp_id',$ref_id);
}
function getEmployee($ref_id){
    return DBHelper::getRecord($this->table,'emp_id',$ref_id);
}
// Update
function updateEmployee($data,$ref_id){
    return DBHelper::updateRecord($this->table,$this->fields,$data,'emp_id',$ref_id); 
 }
 // Delete
 function deleteEmployee($ref_id){
          return DBHelper::deleteRecord($this->table,'emp_id',$ref_id);
}
// Some Functions
    function getCountEmployee(){
        return DBHelper::countRecord('emp_id',$this->table);
    }

}



?>